/********************************************************************************
** Form generated from reading UI file 'userinfo.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERINFO_H
#define UI_USERINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserInfo
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *groupNameLayout;
    QLabel *groupNameLabel;
    QVBoxLayout *nameLayout;
    QLabel *nameLabel;
    QVBoxLayout *loginLayout;
    QLabel *loginLabel;
    QVBoxLayout *middleNameLayout;
    QLabel *middleNameLabel;
    QVBoxLayout *lastNameLayout;
    QLabel *lastNameLabel;
    QVBoxLayout *passwordLayout;
    QLabel *passwordLabel;
    QVBoxLayout *nameUserLayout;
    QLabel *nameUserLabel;
    QVBoxLayout *lastNameUserLayout;
    QLabel *lastNameUserLabel;
    QVBoxLayout *middleNameUserLayout;
    QLabel *middleNameUserLabel;
    QVBoxLayout *groupNameUserLayout;
    QLabel *groupNameUserLabel;
    QVBoxLayout *loginUserLayout;
    QLabel *loginUserLabel;
    QVBoxLayout *passwordUserLayout;
    QLabel *passwordUserLabel;

    void setupUi(QWidget *UserInfo)
    {
        if (UserInfo->objectName().isEmpty())
            UserInfo->setObjectName(QStringLiteral("UserInfo"));
        UserInfo->resize(400, 300);
        gridLayoutWidget = new QWidget(UserInfo);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 0, 391, 291));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        groupNameLayout = new QVBoxLayout();
        groupNameLayout->setObjectName(QStringLiteral("groupNameLayout"));
        groupNameLabel = new QLabel(gridLayoutWidget);
        groupNameLabel->setObjectName(QStringLiteral("groupNameLabel"));

        groupNameLayout->addWidget(groupNameLabel);


        gridLayout->addLayout(groupNameLayout, 3, 0, 1, 1);

        nameLayout = new QVBoxLayout();
        nameLayout->setObjectName(QStringLiteral("nameLayout"));
        nameLabel = new QLabel(gridLayoutWidget);
        nameLabel->setObjectName(QStringLiteral("nameLabel"));

        nameLayout->addWidget(nameLabel);


        gridLayout->addLayout(nameLayout, 0, 0, 1, 1);

        loginLayout = new QVBoxLayout();
        loginLayout->setObjectName(QStringLiteral("loginLayout"));
        loginLabel = new QLabel(gridLayoutWidget);
        loginLabel->setObjectName(QStringLiteral("loginLabel"));

        loginLayout->addWidget(loginLabel);


        gridLayout->addLayout(loginLayout, 4, 0, 1, 1);

        middleNameLayout = new QVBoxLayout();
        middleNameLayout->setObjectName(QStringLiteral("middleNameLayout"));
        middleNameLabel = new QLabel(gridLayoutWidget);
        middleNameLabel->setObjectName(QStringLiteral("middleNameLabel"));

        middleNameLayout->addWidget(middleNameLabel);


        gridLayout->addLayout(middleNameLayout, 2, 0, 1, 1);

        lastNameLayout = new QVBoxLayout();
        lastNameLayout->setObjectName(QStringLiteral("lastNameLayout"));
        lastNameLabel = new QLabel(gridLayoutWidget);
        lastNameLabel->setObjectName(QStringLiteral("lastNameLabel"));

        lastNameLayout->addWidget(lastNameLabel);


        gridLayout->addLayout(lastNameLayout, 1, 0, 1, 1);

        passwordLayout = new QVBoxLayout();
        passwordLayout->setObjectName(QStringLiteral("passwordLayout"));
        passwordLabel = new QLabel(gridLayoutWidget);
        passwordLabel->setObjectName(QStringLiteral("passwordLabel"));

        passwordLayout->addWidget(passwordLabel);


        gridLayout->addLayout(passwordLayout, 5, 0, 1, 1);

        nameUserLayout = new QVBoxLayout();
        nameUserLayout->setObjectName(QStringLiteral("nameUserLayout"));
        nameUserLabel = new QLabel(gridLayoutWidget);
        nameUserLabel->setObjectName(QStringLiteral("nameUserLabel"));

        nameUserLayout->addWidget(nameUserLabel);


        gridLayout->addLayout(nameUserLayout, 0, 1, 1, 1);

        lastNameUserLayout = new QVBoxLayout();
        lastNameUserLayout->setObjectName(QStringLiteral("lastNameUserLayout"));
        lastNameUserLabel = new QLabel(gridLayoutWidget);
        lastNameUserLabel->setObjectName(QStringLiteral("lastNameUserLabel"));

        lastNameUserLayout->addWidget(lastNameUserLabel);


        gridLayout->addLayout(lastNameUserLayout, 1, 1, 1, 1);

        middleNameUserLayout = new QVBoxLayout();
        middleNameUserLayout->setObjectName(QStringLiteral("middleNameUserLayout"));
        middleNameUserLabel = new QLabel(gridLayoutWidget);
        middleNameUserLabel->setObjectName(QStringLiteral("middleNameUserLabel"));

        middleNameUserLayout->addWidget(middleNameUserLabel);


        gridLayout->addLayout(middleNameUserLayout, 2, 1, 1, 1);

        groupNameUserLayout = new QVBoxLayout();
        groupNameUserLayout->setObjectName(QStringLiteral("groupNameUserLayout"));
        groupNameUserLabel = new QLabel(gridLayoutWidget);
        groupNameUserLabel->setObjectName(QStringLiteral("groupNameUserLabel"));

        groupNameUserLayout->addWidget(groupNameUserLabel);


        gridLayout->addLayout(groupNameUserLayout, 3, 1, 1, 1);

        loginUserLayout = new QVBoxLayout();
        loginUserLayout->setObjectName(QStringLiteral("loginUserLayout"));
        loginUserLabel = new QLabel(gridLayoutWidget);
        loginUserLabel->setObjectName(QStringLiteral("loginUserLabel"));

        loginUserLayout->addWidget(loginUserLabel);


        gridLayout->addLayout(loginUserLayout, 4, 1, 1, 1);

        passwordUserLayout = new QVBoxLayout();
        passwordUserLayout->setObjectName(QStringLiteral("passwordUserLayout"));
        passwordUserLabel = new QLabel(gridLayoutWidget);
        passwordUserLabel->setObjectName(QStringLiteral("passwordUserLabel"));

        passwordUserLayout->addWidget(passwordUserLabel);


        gridLayout->addLayout(passwordUserLayout, 5, 1, 1, 1);


        retranslateUi(UserInfo);

        QMetaObject::connectSlotsByName(UserInfo);
    } // setupUi

    void retranslateUi(QWidget *UserInfo)
    {
        UserInfo->setWindowTitle(QApplication::translate("UserInfo", "Form", 0));
        groupNameLabel->setText(QApplication::translate("UserInfo", "\320\223\321\200\321\203\320\277\320\277\320\260:", 0));
        nameLabel->setText(QApplication::translate("UserInfo", "\320\230\320\274\321\217:", 0));
        loginLabel->setText(QApplication::translate("UserInfo", "\320\233\320\276\320\263\320\270\320\275:", 0));
        middleNameLabel->setText(QApplication::translate("UserInfo", "\320\236\321\202\321\207\320\265\321\201\321\202\320\262\320\276:", 0));
        lastNameLabel->setText(QApplication::translate("UserInfo", "\320\244\320\260\320\274\320\270\320\273\320\270\321\217:", 0));
        passwordLabel->setText(QApplication::translate("UserInfo", "\320\237\320\260\321\200\320\276\320\273\321\214:", 0));
        nameUserLabel->setText(QString());
        lastNameUserLabel->setText(QString());
        middleNameUserLabel->setText(QString());
        groupNameUserLabel->setText(QString());
        loginUserLabel->setText(QString());
        passwordUserLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class UserInfo: public Ui_UserInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERINFO_H
