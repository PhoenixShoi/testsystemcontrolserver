/****************************************************************************
** Meta object code from reading C++ file 'adduser.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TestSystemControlServer/adduser.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'adduser.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_AddUser_t {
    QByteArrayData data[10];
    char stringdata[143];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AddUser_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AddUser_t qt_meta_stringdata_AddUser = {
    {
QT_MOC_LITERAL(0, 0, 7), // "AddUser"
QT_MOC_LITERAL(1, 8, 3), // "add"
QT_MOC_LITERAL(2, 12, 0), // ""
QT_MOC_LITERAL(3, 13, 4), // "User"
QT_MOC_LITERAL(4, 18, 11), // "updateTable"
QT_MOC_LITERAL(5, 30, 29), // "on_loginCheckBox_stateChanged"
QT_MOC_LITERAL(6, 60, 4), // "arg1"
QT_MOC_LITERAL(7, 65, 32), // "on_passwordCheckBox_stateChanged"
QT_MOC_LITERAL(8, 98, 23), // "on_cancelButton_clicked"
QT_MOC_LITERAL(9, 122, 20) // "on_addButton_clicked"

    },
    "AddUser\0add\0\0User\0updateTable\0"
    "on_loginCheckBox_stateChanged\0arg1\0"
    "on_passwordCheckBox_stateChanged\0"
    "on_cancelButton_clicked\0on_addButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AddUser[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    0,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   48,    2, 0x08 /* Private */,
       7,    1,   51,    2, 0x08 /* Private */,
       8,    0,   54,    2, 0x08 /* Private */,
       9,    0,   55,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void AddUser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AddUser *_t = static_cast<AddUser *>(_o);
        switch (_id) {
        case 0: _t->add((*reinterpret_cast< User(*)>(_a[1]))); break;
        case 1: _t->updateTable(); break;
        case 2: _t->on_loginCheckBox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_passwordCheckBox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_cancelButton_clicked(); break;
        case 5: _t->on_addButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (AddUser::*_t)(User );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AddUser::add)) {
                *result = 0;
            }
        }
        {
            typedef void (AddUser::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&AddUser::updateTable)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject AddUser::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AddUser.data,
      qt_meta_data_AddUser,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *AddUser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AddUser::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_AddUser.stringdata))
        return static_cast<void*>(const_cast< AddUser*>(this));
    return QDialog::qt_metacast(_clname);
}

int AddUser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void AddUser::add(User _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void AddUser::updateTable()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
