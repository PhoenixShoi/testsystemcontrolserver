/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *functionAction;
    QAction *addAction;
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *programLayout;
    QVBoxLayout *functionsLayout;
    QHBoxLayout *upLayout;
    QVBoxLayout *listOnlineAndOfflineLayout;
    QVBoxLayout *onlineLayout;
    QLabel *onlineLabel;
    QTableView *onlineTableView;
    QVBoxLayout *offlineLayout;
    QLabel *offlineLabel;
    QTableView *offlineTableView;
    QVBoxLayout *funcLayout;
    QPushButton *addButton;
    QVBoxLayout *questionSelectLayout;
    QVBoxLayout *numberLayout;
    QLCDNumber *questionNumber;
    QHBoxLayout *buttonSelectNumberLayout;
    QVBoxLayout *prevButtonLayout;
    QPushButton *prevButton;
    QVBoxLayout *nextButtonLayout;
    QPushButton *nextButton;
    QVBoxLayout *answerLayout;
    QTableWidget *answerTableWidget;
    QMenuBar *menuBar;
    QMenu *menu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(674, 667);
        functionAction = new QAction(MainWindow);
        functionAction->setObjectName(QStringLiteral("functionAction"));
        addAction = new QAction(MainWindow);
        addAction->setObjectName(QStringLiteral("addAction"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(-1, -1, 671, 635));
        programLayout = new QVBoxLayout(verticalLayoutWidget);
        programLayout->setSpacing(6);
        programLayout->setContentsMargins(11, 11, 11, 11);
        programLayout->setObjectName(QStringLiteral("programLayout"));
        programLayout->setContentsMargins(0, 0, 0, 0);
        functionsLayout = new QVBoxLayout();
        functionsLayout->setSpacing(6);
        functionsLayout->setObjectName(QStringLiteral("functionsLayout"));
        upLayout = new QHBoxLayout();
        upLayout->setSpacing(6);
        upLayout->setObjectName(QStringLiteral("upLayout"));
        listOnlineAndOfflineLayout = new QVBoxLayout();
        listOnlineAndOfflineLayout->setSpacing(6);
        listOnlineAndOfflineLayout->setObjectName(QStringLiteral("listOnlineAndOfflineLayout"));
        onlineLayout = new QVBoxLayout();
        onlineLayout->setSpacing(6);
        onlineLayout->setObjectName(QStringLiteral("onlineLayout"));
        onlineLabel = new QLabel(verticalLayoutWidget);
        onlineLabel->setObjectName(QStringLiteral("onlineLabel"));

        onlineLayout->addWidget(onlineLabel);

        onlineTableView = new QTableView(verticalLayoutWidget);
        onlineTableView->setObjectName(QStringLiteral("onlineTableView"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(onlineTableView->sizePolicy().hasHeightForWidth());
        onlineTableView->setSizePolicy(sizePolicy);

        onlineLayout->addWidget(onlineTableView);


        listOnlineAndOfflineLayout->addLayout(onlineLayout);

        offlineLayout = new QVBoxLayout();
        offlineLayout->setSpacing(6);
        offlineLayout->setObjectName(QStringLiteral("offlineLayout"));
        offlineLabel = new QLabel(verticalLayoutWidget);
        offlineLabel->setObjectName(QStringLiteral("offlineLabel"));

        offlineLayout->addWidget(offlineLabel);

        offlineTableView = new QTableView(verticalLayoutWidget);
        offlineTableView->setObjectName(QStringLiteral("offlineTableView"));
        sizePolicy.setHeightForWidth(offlineTableView->sizePolicy().hasHeightForWidth());
        offlineTableView->setSizePolicy(sizePolicy);

        offlineLayout->addWidget(offlineTableView);


        listOnlineAndOfflineLayout->addLayout(offlineLayout);


        upLayout->addLayout(listOnlineAndOfflineLayout);

        funcLayout = new QVBoxLayout();
        funcLayout->setSpacing(6);
        funcLayout->setObjectName(QStringLiteral("funcLayout"));
        addButton = new QPushButton(verticalLayoutWidget);
        addButton->setObjectName(QStringLiteral("addButton"));

        funcLayout->addWidget(addButton);


        upLayout->addLayout(funcLayout);

        questionSelectLayout = new QVBoxLayout();
        questionSelectLayout->setSpacing(6);
        questionSelectLayout->setObjectName(QStringLiteral("questionSelectLayout"));
        numberLayout = new QVBoxLayout();
        numberLayout->setSpacing(6);
        numberLayout->setObjectName(QStringLiteral("numberLayout"));
        questionNumber = new QLCDNumber(verticalLayoutWidget);
        questionNumber->setObjectName(QStringLiteral("questionNumber"));
        questionNumber->setProperty("intValue", QVariant(1));

        numberLayout->addWidget(questionNumber);


        questionSelectLayout->addLayout(numberLayout);

        buttonSelectNumberLayout = new QHBoxLayout();
        buttonSelectNumberLayout->setSpacing(6);
        buttonSelectNumberLayout->setObjectName(QStringLiteral("buttonSelectNumberLayout"));
        prevButtonLayout = new QVBoxLayout();
        prevButtonLayout->setSpacing(6);
        prevButtonLayout->setObjectName(QStringLiteral("prevButtonLayout"));
        prevButton = new QPushButton(verticalLayoutWidget);
        prevButton->setObjectName(QStringLiteral("prevButton"));
        prevButton->setEnabled(false);

        prevButtonLayout->addWidget(prevButton);


        buttonSelectNumberLayout->addLayout(prevButtonLayout);

        nextButtonLayout = new QVBoxLayout();
        nextButtonLayout->setSpacing(6);
        nextButtonLayout->setObjectName(QStringLiteral("nextButtonLayout"));
        nextButton = new QPushButton(verticalLayoutWidget);
        nextButton->setObjectName(QStringLiteral("nextButton"));

        nextButtonLayout->addWidget(nextButton);


        buttonSelectNumberLayout->addLayout(nextButtonLayout);


        questionSelectLayout->addLayout(buttonSelectNumberLayout);


        upLayout->addLayout(questionSelectLayout);


        functionsLayout->addLayout(upLayout);


        programLayout->addLayout(functionsLayout);

        answerLayout = new QVBoxLayout();
        answerLayout->setSpacing(6);
        answerLayout->setObjectName(QStringLiteral("answerLayout"));
        answerTableWidget = new QTableWidget(verticalLayoutWidget);
        if (answerTableWidget->columnCount() < 6)
            answerTableWidget->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        answerTableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        answerTableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        answerTableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        answerTableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        answerTableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        answerTableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        answerTableWidget->setObjectName(QStringLiteral("answerTableWidget"));

        answerLayout->addWidget(answerTableWidget);


        programLayout->addLayout(answerLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 674, 22));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menu->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        functionAction->setText(QApplication::translate("MainWindow", "\320\244\321\203\320\275\320\272\321\206\320\270\320\270", 0));
        addAction->setText(QApplication::translate("MainWindow", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0));
        onlineLabel->setText(QApplication::translate("MainWindow", "\320\236\320\275\320\273\320\260\320\271\320\275", 0));
        offlineLabel->setText(QApplication::translate("MainWindow", "\320\236\321\204\321\204\320\273\320\260\320\271\320\275", 0));
        addButton->setText(QApplication::translate("MainWindow", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0));
        prevButton->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\265\320\264\321\213\320\264\321\203\321\211\320\270\320\271", 0));
        nextButton->setText(QApplication::translate("MainWindow", "\320\241\320\273\320\265\320\264\321\203\321\216\321\211\320\270\320\271", 0));
        QTableWidgetItem *___qtablewidgetitem = answerTableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "\320\244\320\260\320\274\320\270\320\273\320\270\321\217", 0));
        QTableWidgetItem *___qtablewidgetitem1 = answerTableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "\320\230\320\274\321\217", 0));
        QTableWidgetItem *___qtablewidgetitem2 = answerTableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "\320\236\321\202\321\207\320\265\321\201\321\202\320\262\320\276", 0));
        QTableWidgetItem *___qtablewidgetitem3 = answerTableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "\320\223\321\200\321\203\320\277\320\277\320\260", 0));
        QTableWidgetItem *___qtablewidgetitem4 = answerTableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "\320\235\320\276\320\274\320\265\321\200 \320\262\320\276\320\277\321\200\320\276\321\201\320\260", 0));
        QTableWidgetItem *___qtablewidgetitem5 = answerTableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "\320\235\320\276\320\274\320\265\321\200 \320\276\321\202\320\262\320\265\321\202\320\260", 0));
        menu->setTitle(QApplication::translate("MainWindow", "\320\244\321\203\320\275\320\272\321\206\320\270\320\270", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
