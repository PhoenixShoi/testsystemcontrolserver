/********************************************************************************
** Form generated from reading UI file 'adduser.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDUSER_H
#define UI_ADDUSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddUser
{
public:
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *mainLayout;
    QVBoxLayout *formLayout;
    QVBoxLayout *nameLayout;
    QVBoxLayout *nameLabelLayout;
    QLabel *nameLabel;
    QVBoxLayout *nameEditLayout;
    QLineEdit *nameLineEdit;
    QVBoxLayout *lastNameLayout;
    QVBoxLayout *lastNameLabelLayout;
    QLabel *lastNameLabel;
    QVBoxLayout *lastNameEditLayout;
    QLineEdit *lastNameEdit;
    QVBoxLayout *middleNameLayout;
    QVBoxLayout *middleNameLabelLayout;
    QLabel *middleNameLabel;
    QVBoxLayout *middleNameEditLayout;
    QLineEdit *middleNameEdit;
    QVBoxLayout *groupNameLayout;
    QVBoxLayout *groupLabelLayout;
    QLabel *groupLabel;
    QVBoxLayout *groupEditLayout;
    QLineEdit *groupEdit;
    QHBoxLayout *loginLayout;
    QVBoxLayout *loginCheckBoxLayout;
    QCheckBox *loginCheckBox;
    QVBoxLayout *loginEditLayout;
    QLineEdit *loginLineEdit;
    QHBoxLayout *passwordLayout;
    QVBoxLayout *passwordCheckBoxLayout;
    QCheckBox *passwordCheckBox;
    QVBoxLayout *passwordEditLayout;
    QLineEdit *passwordEdit;
    QHBoxLayout *buttonLayout;
    QVBoxLayout *buttonCancelLayout;
    QPushButton *cancelButton;
    QVBoxLayout *buttonAddLayout;
    QPushButton *addButton;

    void setupUi(QDialog *AddUser)
    {
        if (AddUser->objectName().isEmpty())
            AddUser->setObjectName(QStringLiteral("AddUser"));
        AddUser->resize(501, 427);
        verticalLayoutWidget_3 = new QWidget(AddUser);
        verticalLayoutWidget_3->setObjectName(QStringLiteral("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(10, 10, 481, 411));
        mainLayout = new QVBoxLayout(verticalLayoutWidget_3);
        mainLayout->setObjectName(QStringLiteral("mainLayout"));
        mainLayout->setContentsMargins(0, 0, 0, 0);
        formLayout = new QVBoxLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        nameLayout = new QVBoxLayout();
        nameLayout->setObjectName(QStringLiteral("nameLayout"));
        nameLabelLayout = new QVBoxLayout();
        nameLabelLayout->setObjectName(QStringLiteral("nameLabelLayout"));
        nameLabel = new QLabel(verticalLayoutWidget_3);
        nameLabel->setObjectName(QStringLiteral("nameLabel"));

        nameLabelLayout->addWidget(nameLabel);


        nameLayout->addLayout(nameLabelLayout);

        nameEditLayout = new QVBoxLayout();
        nameEditLayout->setObjectName(QStringLiteral("nameEditLayout"));
        nameLineEdit = new QLineEdit(verticalLayoutWidget_3);
        nameLineEdit->setObjectName(QStringLiteral("nameLineEdit"));

        nameEditLayout->addWidget(nameLineEdit);


        nameLayout->addLayout(nameEditLayout);


        formLayout->addLayout(nameLayout);

        lastNameLayout = new QVBoxLayout();
        lastNameLayout->setObjectName(QStringLiteral("lastNameLayout"));
        lastNameLabelLayout = new QVBoxLayout();
        lastNameLabelLayout->setObjectName(QStringLiteral("lastNameLabelLayout"));
        lastNameLabel = new QLabel(verticalLayoutWidget_3);
        lastNameLabel->setObjectName(QStringLiteral("lastNameLabel"));

        lastNameLabelLayout->addWidget(lastNameLabel);

        lastNameEditLayout = new QVBoxLayout();
        lastNameEditLayout->setObjectName(QStringLiteral("lastNameEditLayout"));
        lastNameEdit = new QLineEdit(verticalLayoutWidget_3);
        lastNameEdit->setObjectName(QStringLiteral("lastNameEdit"));

        lastNameEditLayout->addWidget(lastNameEdit);


        lastNameLabelLayout->addLayout(lastNameEditLayout);


        lastNameLayout->addLayout(lastNameLabelLayout);


        formLayout->addLayout(lastNameLayout);

        middleNameLayout = new QVBoxLayout();
        middleNameLayout->setObjectName(QStringLiteral("middleNameLayout"));
        middleNameLabelLayout = new QVBoxLayout();
        middleNameLabelLayout->setObjectName(QStringLiteral("middleNameLabelLayout"));
        middleNameLabel = new QLabel(verticalLayoutWidget_3);
        middleNameLabel->setObjectName(QStringLiteral("middleNameLabel"));

        middleNameLabelLayout->addWidget(middleNameLabel);

        middleNameEditLayout = new QVBoxLayout();
        middleNameEditLayout->setObjectName(QStringLiteral("middleNameEditLayout"));
        middleNameEdit = new QLineEdit(verticalLayoutWidget_3);
        middleNameEdit->setObjectName(QStringLiteral("middleNameEdit"));

        middleNameEditLayout->addWidget(middleNameEdit);


        middleNameLabelLayout->addLayout(middleNameEditLayout);


        middleNameLayout->addLayout(middleNameLabelLayout);


        formLayout->addLayout(middleNameLayout);

        groupNameLayout = new QVBoxLayout();
        groupNameLayout->setObjectName(QStringLiteral("groupNameLayout"));
        groupLabelLayout = new QVBoxLayout();
        groupLabelLayout->setObjectName(QStringLiteral("groupLabelLayout"));
        groupLabel = new QLabel(verticalLayoutWidget_3);
        groupLabel->setObjectName(QStringLiteral("groupLabel"));

        groupLabelLayout->addWidget(groupLabel);


        groupNameLayout->addLayout(groupLabelLayout);

        groupEditLayout = new QVBoxLayout();
        groupEditLayout->setObjectName(QStringLiteral("groupEditLayout"));
        groupEdit = new QLineEdit(verticalLayoutWidget_3);
        groupEdit->setObjectName(QStringLiteral("groupEdit"));

        groupEditLayout->addWidget(groupEdit);


        groupNameLayout->addLayout(groupEditLayout);


        formLayout->addLayout(groupNameLayout);

        loginLayout = new QHBoxLayout();
        loginLayout->setObjectName(QStringLiteral("loginLayout"));
        loginCheckBoxLayout = new QVBoxLayout();
        loginCheckBoxLayout->setObjectName(QStringLiteral("loginCheckBoxLayout"));
        loginCheckBox = new QCheckBox(verticalLayoutWidget_3);
        loginCheckBox->setObjectName(QStringLiteral("loginCheckBox"));
        loginCheckBox->setEnabled(true);
        loginCheckBox->setChecked(true);

        loginCheckBoxLayout->addWidget(loginCheckBox);


        loginLayout->addLayout(loginCheckBoxLayout);

        loginEditLayout = new QVBoxLayout();
        loginEditLayout->setObjectName(QStringLiteral("loginEditLayout"));
        loginLineEdit = new QLineEdit(verticalLayoutWidget_3);
        loginLineEdit->setObjectName(QStringLiteral("loginLineEdit"));
        loginLineEdit->setEnabled(false);

        loginEditLayout->addWidget(loginLineEdit);


        loginLayout->addLayout(loginEditLayout);


        formLayout->addLayout(loginLayout);

        passwordLayout = new QHBoxLayout();
        passwordLayout->setObjectName(QStringLiteral("passwordLayout"));
        passwordCheckBoxLayout = new QVBoxLayout();
        passwordCheckBoxLayout->setObjectName(QStringLiteral("passwordCheckBoxLayout"));
        passwordCheckBox = new QCheckBox(verticalLayoutWidget_3);
        passwordCheckBox->setObjectName(QStringLiteral("passwordCheckBox"));
        passwordCheckBox->setChecked(true);

        passwordCheckBoxLayout->addWidget(passwordCheckBox);


        passwordLayout->addLayout(passwordCheckBoxLayout);

        passwordEditLayout = new QVBoxLayout();
        passwordEditLayout->setObjectName(QStringLiteral("passwordEditLayout"));
        passwordEdit = new QLineEdit(verticalLayoutWidget_3);
        passwordEdit->setObjectName(QStringLiteral("passwordEdit"));
        passwordEdit->setEnabled(false);
        passwordEdit->setEchoMode(QLineEdit::Password);

        passwordEditLayout->addWidget(passwordEdit);


        passwordLayout->addLayout(passwordEditLayout);


        formLayout->addLayout(passwordLayout);


        mainLayout->addLayout(formLayout);

        buttonLayout = new QHBoxLayout();
        buttonLayout->setObjectName(QStringLiteral("buttonLayout"));
        buttonCancelLayout = new QVBoxLayout();
        buttonCancelLayout->setObjectName(QStringLiteral("buttonCancelLayout"));
        cancelButton = new QPushButton(verticalLayoutWidget_3);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        buttonCancelLayout->addWidget(cancelButton);


        buttonLayout->addLayout(buttonCancelLayout);

        buttonAddLayout = new QVBoxLayout();
        buttonAddLayout->setObjectName(QStringLiteral("buttonAddLayout"));
        addButton = new QPushButton(verticalLayoutWidget_3);
        addButton->setObjectName(QStringLiteral("addButton"));

        buttonAddLayout->addWidget(addButton);


        buttonLayout->addLayout(buttonAddLayout);


        mainLayout->addLayout(buttonLayout);

        verticalLayoutWidget_3->raise();
        groupLabel->raise();

        retranslateUi(AddUser);

        QMetaObject::connectSlotsByName(AddUser);
    } // setupUi

    void retranslateUi(QDialog *AddUser)
    {
        AddUser->setWindowTitle(QApplication::translate("AddUser", "Dialog", 0));
        nameLabel->setText(QApplication::translate("AddUser", "\320\230\320\274\321\217", 0));
        lastNameLabel->setText(QApplication::translate("AddUser", "\320\244\320\260\320\274\320\270\320\273\320\270\321\217", 0));
        middleNameLabel->setText(QApplication::translate("AddUser", "\320\236\321\202\321\207\320\265\321\201\321\202\320\262\320\276", 0));
        groupLabel->setText(QApplication::translate("AddUser", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265 \320\263\321\200\321\203\320\277\320\277\321\213", 0));
        loginCheckBox->setText(QApplication::translate("AddUser", "\320\223\320\265\320\275\320\265\321\200\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\273\320\276\320\263\320\270\320\275", 0));
        passwordCheckBox->setText(QApplication::translate("AddUser", "\320\223\320\265\320\275\320\265\321\200\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\277\320\260\321\200\320\276\320\273\321\214", 0));
        cancelButton->setText(QApplication::translate("AddUser", "\320\236\321\202\320\274\320\265\320\275\320\260", 0));
        addButton->setText(QApplication::translate("AddUser", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0));
    } // retranslateUi

};

namespace Ui {
    class AddUser: public Ui_AddUser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDUSER_H
