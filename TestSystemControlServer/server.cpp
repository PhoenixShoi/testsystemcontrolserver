#include "server.h"
#include <QMessageBox>
#include <QString>
#include <QDebug>

Server::Server()
{
    m_ptcpServer = new QTcpServer();
    if(!m_ptcpServer->listen(QHostAddress::Any,2326))
    {
        QMessageBox::critical(0,"Server Error","Unable tp start the server:"+m_ptcpServer->errorString());
        m_ptcpServer->close();
        return;
    }
    connect(m_ptcpServer,SIGNAL(newConnection()),this,SLOT(slotNewConnection()));
}

void Server::sendToClient(QTcpSocket *pSocket, const QString str)
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock,QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_4);
    out<<str.length()*2;
    for(int i=0;i<str.length();i++)
        out<<quint16(str.at(i).toLatin1());
    pSocket->write(arrBlock);
}

QString Server::runCommand(QString str)
{
    if(str[0]=='0')
    {
        return "0";
    }
    if(str[0]=='1')
    {
        int i=2;
        QString login="",password="";
        for(;i<str.size();i++)
            if(str[i]!='_')
            {
                login+=str[i];
            }
            else
                break;
        for(i++;i<str.size();i++)
        {
            password+=str[i];
        }
        emit newUserConnect(login,password);
        if(flagUserAccept){
            flagUserAccept=false;
            return "-1";
        }
        else
            return "0";
    }
    if(str[0]=='2')
    {
        int i=2;
        QString login="",password="",answer="";
        for(;i<str.size();i++)
            if(str[i]!='_')
            {
                login+=str[i];
            }
            else
                break;
        for(i++;i<str.size();i++)
            if(str[i]!='_')
            {
                password+=str[i];
            }
            else
                break;
        for(i++;i<str.size();i++)
        {
            answer+=str[i];
        }
        emit newUserAnswer(login,password,answer);
        return "-2";
    }
}

void Server::userAccept()
{
    this->flagUserAccept=true;
}

void Server::slotNewConnection()
{
    QTcpSocket *pClientSocket = m_ptcpServer->nextPendingConnection();
    connect(pClientSocket,SIGNAL(disconnected()),pClientSocket,SLOT(deleteLater()));
    connect(pClientSocket,SIGNAL(readyRead()),this,SLOT(slotReadClient()));
}

void Server::slotReadClient()
{
    QTcpSocket *pClientSocket = (QTcpSocket*) sender();
    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_4);
    QString str="";
    for(;;)
    {
        if(!m_nNextBlockSize)
        {
            qDebug()<<pClientSocket->bytesAvailable();
            if(pClientSocket->bytesAvailable()<sizeof(quint16))
            {
                break;
            }
            in>>m_nNextBlockSize;
            qDebug()<<pClientSocket->bytesAvailable();
            while(pClientSocket->bytesAvailable()){
                qDebug()<<pClientSocket->bytesAvailable();
                quint8 symb;
                in>>symb;
                str+=(QChar)symb;
            }
            qDebug()<<pClientSocket->bytesAvailable();
            sendToClient(pClientSocket,(QString)runCommand(str));
        }
        m_nNextBlockSize=0;
    }
}

