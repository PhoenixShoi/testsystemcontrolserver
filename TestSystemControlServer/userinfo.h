#ifndef USERINFO_H
#define USERINFO_H

#include <QWidget>
#include "user.h"

namespace Ui {
class UserInfo;
}

class UserInfo : public QWidget
{
    Q_OBJECT

public:
    void update(User);
    explicit UserInfo(QWidget *parent = 0);
    ~UserInfo();

private slots:
    void on_deleteButton_clicked();

private:
    Ui::UserInfo *ui;

signals:
    void deleteUser(QString);
};

#endif // USERINFO_H
