#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adduser.h"
#include "userinfo.h"
#include "exportfile.h"
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QMessageBox>
#include <QDebug>
#include <QSqlTableModel>
#include <QTcpSocket>
#include <QTime>
#include <QSqlRecord>
#include <QDir>
#include <QTableView>
#include <QStandardItemModel>
#include <QtCore>
#include <QFileDialog>
#include <QTextCodec>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    core = new Server;
    createConnection();
    userOffline();
    fullListUpdate();

    connect(core,SIGNAL(newUserConnect(QString,QString)),this,SLOT(loginUser(QString,QString)));
    connect(core,SIGNAL(newUserAnswer(QString,QString,QString)),this,SLOT(newAnswer(QString,QString,QString)));

    ui->answerTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->answerTableWidget->setRowCount(0);
    ui->answerTableWidget->setColumnCount(6);

    this->setWindowTitle("Test System Control");
}

MainWindow::~MainWindow()
{
    delete ui;
    myDB.close();
}

void MainWindow::add(User newUser)
{
    this->insertData(newUser);
    updateOffline();
}

void MainWindow::exportFile(QString strNameFile)
{

    QString dirPath="";
    while(!dirPath.length())
        dirPath=QFileDialog::getExistingDirectory(this, tr("Открыть папку."),
                                                      "/home",
                                                      QFileDialog::ShowDirsOnly
                                                      | QFileDialog::DontResolveSymlinks);
    QFile exportFile(dirPath+"\/"+strNameFile+".csv");

    if(exportFile.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream data(&exportFile);

        data.setCodec("cp1251");

        QStringList strList;

        for(int r=0;r<ui->answerTableWidget->rowCount();r++)
        {
            strList.clear();
            for(int c=0;c<ui->answerTableWidget->columnCount();c++)
            {
                strList << "\" "+ui->answerTableWidget->item( r, c )->text()+"\" ";
            }
            data << strList.join( ";" )+"\n";
        }
        exportFile.close();
    }

}

void MainWindow::deleteUser(QString loginDeleteUser)
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("DELETE FROM Users WHERE login='"+loginDeleteUser+"'");
    fullListUpdate();
}

void MainWindow::createConnection()
{
    myDB = QSqlDatabase::addDatabase("QSQLITE");
    QString pathApplication=QCoreApplication::applicationDirPath();
    myDB.setDatabaseName(pathApplication+"/testcontrol.sqlite");
    qDebug()<<QDir::currentPath();
    myDB.open();
    if(!myDB.isOpen())
    {
        QMessageBox mb;
        mb.setText("База отсутствует");
        mb.setIcon(QMessageBox::Information);
        mb.exec();
    }
}

void MainWindow::insertData(User newUser)
{
    QSqlQuery insertQuery;
    insertQuery.prepare("INSERT INTO Users(name,lastName,middleName,groupName,login,password)"
                   "VALUES(:name,:lastName,:middleName,:groupName,:login,:password);");
    insertQuery.bindValue(":name",newUser.getName());
    insertQuery.bindValue(":lastName",newUser.getLastName());
    insertQuery.bindValue(":middleName",newUser.getMiddleName());
    insertQuery.bindValue(":groupName",newUser.getNameGroup());
    insertQuery.bindValue(":login",newUser.getLogin());
    insertQuery.bindValue(":password",newUser.getPassword());
    if(!insertQuery.exec())
    {
        QMessageBox mb;
        mb.setText("Не могу вставить!");
        mb.setIcon(QMessageBox::Information);
        mb.exec();
    }

}

void MainWindow::updateOffline()
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT name, lastName, groupName FROM Users WHERE status=0");
    ui->offlineTableView->setModel(model);
    model->setHeaderData(0,Qt::Horizontal,tr("Имя"));
    model->setHeaderData(1,Qt::Horizontal,tr("Фамилия"));
    model->setHeaderData(2,Qt::Horizontal,tr("Группа"));
    ui->offlineTableView->update();
}

void MainWindow::updateOnline()
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT name, lastName, groupName FROM Users WHERE status=1");
    ui->onlineTableView->setModel(model);
    model->setHeaderData(0,Qt::Horizontal,tr("Имя"));
    model->setHeaderData(1,Qt::Horizontal,tr("Фамилия"));
    model->setHeaderData(2,Qt::Horizontal,tr("Группа"));
    ui->onlineTableView->update();
}

void MainWindow::fullListUpdate()
{
    updateOffline();
    updateOnline();
}

void MainWindow::openUser(QString nameUser,QString nameLastUser)
{
    User profile("","","","","","");
    QSqlQuery *query = new QSqlQuery();
    query->prepare("SELECT * FROM Users WHERE name='"+nameUser+"' AND lastName='"+nameLastUser+"'");
    query->exec();
    QSqlRecord rec = query->record();
    while(query->next())
    {
        profile.setName(query->value(rec.indexOf("name")).toString());
        profile.setLastName(query->value(rec.indexOf("lastName")).toString());
        profile.setMiddleName(query->value(rec.indexOf("middleName")).toString());
        profile.setNameGroup(query->value(rec.indexOf("groupName")).toString());
        profile.setLogin(query->value(rec.indexOf("login")).toString());
        profile.setPassword(query->value(rec.indexOf("password")).toString());
    }
    UserInfo *info= new UserInfo;
    connect(info,SIGNAL(deleteUser(QString)),this,SLOT(deleteUser(QString)));
    info->update(profile);
    info->show();
}

void MainWindow::userOffline()
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("UPDATE Users SET status=0 WHERE status=1");
}

void MainWindow::addAnswerInfo(QString str, int x)
{
    QTableWidgetItem *elm=new QTableWidgetItem;
    elm->setText(str);
    ui->answerTableWidget->setItem(ui->answerTableWidget->rowCount()-1,x,elm);
}

void MainWindow::on_prevButton_clicked()
{
    if(ui->questionNumber->intValue()==2)
        ui->prevButton->setEnabled(false);
    ui->questionNumber->display(ui->questionNumber->intValue()-1);
}

void MainWindow::on_nextButton_clicked()
{
    if(ui->questionNumber->intValue()==1)
        ui->prevButton->setEnabled(true);
    ui->questionNumber->display(ui->questionNumber->intValue()+1);
}

void MainWindow::on_onlineTableView_doubleClicked(const QModelIndex &index)
{
    if(!index.column())
    {
        openUser(index.data().toString(),index.sibling(index.row(),index.column()+1).data().toString());
    }
}

void MainWindow::on_offlineTableView_doubleClicked(const QModelIndex &index)
{
    if(!index.column())
    {
        openUser(index.data().toString(),index.sibling(index.row(),index.column()+1).data().toString());
    }
}

void MainWindow::on_addButton_clicked()
{
    AddUser *user = new AddUser;
    connect(user,SIGNAL(add(User)),this,SLOT(add(User)));
    user->show();
}

void MainWindow::loginUser(QString login, QString password)
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT name FROM Users WHERE login='"+login+"' AND password='"+password+"'");
    if(model->rowCount()==1)
    {
        core->userAccept();
        model->setQuery("UPDATE Users SET status=1 WHERE login='"+login+"' AND password='"+password+"'");
        fullListUpdate();
    }
}

void MainWindow::newAnswer(QString login, QString password, QString answer)
{
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT name, lastName, middleName, groupName FROM Users WHERE login='"+login+"' AND password='"+password+"' AND status=1");
    if(model->rowCount()==1)
    {
        qDebug()<<ui->answerTableWidget->rowCount();
        ui->answerTableWidget->setRowCount(ui->answerTableWidget->rowCount()+1);
        int v=0;
        addAnswerInfo(model->record(v).value("lastName").toString().toUtf8(),0);
        addAnswerInfo(model->record(v).value("name").toString().toUtf8(),1);
        addAnswerInfo(model->record(v).value("middleName").toString().toUtf8(),2);
        addAnswerInfo(model->record(v).value("groupName").toString().toUtf8(),3);
        addAnswerInfo(QString::number(ui->questionNumber->value()).toUtf8(),4);
        addAnswerInfo(answer.toUtf8(),5);
    }
}

void MainWindow::on_exportButton_clicked()
{
    ExportFile *newWindow = new ExportFile;
    connect(newWindow,SIGNAL(exportFile(QString)),this,SLOT(exportFile(QString)));
    newWindow->show();
}
