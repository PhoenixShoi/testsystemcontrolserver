#-------------------------------------------------
#
# Project created by QtCreator 2015-05-26T22:28:54
#
#-------------------------------------------------

QT       += core gui sql network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestSystemControlServer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    adduser.cpp \
    user.cpp \
    userinfo.cpp \
    server.cpp \
    exportfile.cpp

HEADERS  += mainwindow.h \
    adduser.h \
    user.h \
    userinfo.h \
    server.h \
    exportfile.h

CONFIG += c++11

FORMS    += mainwindow.ui \
    adduser.ui \
    userinfo.ui \
    exportfile.ui

RESOURCES +=

DISTFILES += \
    testcontrol.sqlite
