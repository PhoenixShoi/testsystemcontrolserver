#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QTime>

class Server : public QObject
{
Q_OBJECT
public:
    Server();
    void userAccept();
private:

    bool flagUserAccept=false;

    QTcpServer *m_ptcpServer;
    quint8 m_nNextBlockSize=0;

    void sendToClient(QTcpSocket *pSocket,const QString str);

    QString runCommand(QString str);
public slots:
    void slotNewConnection();
    void slotReadClient();
signals:
    void newUserConnect(QString login,QString password);
    void newUserAnswer(QString login,QString password,QString answer);
};

#endif // SERVER_H
