#ifndef EXPORTFILE_H
#define EXPORTFILE_H

#include <QWidget>
#include <QString>

namespace Ui {
class ExportFile;
}

class ExportFile : public QWidget
{
    Q_OBJECT

public:
    explicit ExportFile(QWidget *parent = 0);
    ~ExportFile();

private slots:
    void on_exportButton_clicked();

private:
    Ui::ExportFile *ui;
    void callMessageBox(QString str);
signals:
    void exportFile(QString);
};

#endif // EXPORTFILE_H
