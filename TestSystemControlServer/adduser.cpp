#include "adduser.h"
#include "ui_adduser.h"
#include <QMessageBox>

AddUser::AddUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddUser)
{
    ui->setupUi(this);
    generateLoginEdit();
    generatePasswordEdit();

    this->setWindowTitle("Добавить пользователя");
}

AddUser::~AddUser()
{
    delete ui;
}

void AddUser::on_loginCheckBox_stateChanged(int arg1)
{
    if(arg1==0){
        ui->loginLineEdit->setEnabled(true);
        ui->loginLineEdit->clear();
    }
    else{
        ui->loginLineEdit->setEnabled(false);
        generateLoginEdit();
    }
}

void AddUser::on_passwordCheckBox_stateChanged(int arg1)
{
    if(arg1==0){
        ui->passwordEdit->setEnabled(true);
        ui->passwordEdit->clear();
    }
    else{
        ui->passwordEdit->setEnabled(false);
        generatePasswordEdit();
    }
}

void AddUser::generateLoginEdit()
{
    ui->loginLineEdit->setText(generateString());
}

void AddUser::generatePasswordEdit()
{
    ui->passwordEdit->setText(generateString());
}

QString AddUser::generateString()
{
    int symbolsCount=1;
    while(symbolsCount<8)
    {
        symbolsCount=8+qrand()%4;
    }
    QString str="";
    str.resize(symbolsCount);
    for(int i=0;i<symbolsCount;++i)
    {
        str[i]=QChar('0'+char(qrand()%('9'-'0')));
    }
    return str;
}

bool AddUser::checkInfo()
{
    if(!ui->nameLineEdit->text().length())
        return 0;
    if(!ui->lastNameEdit->text().length())
        return 0;
    if(!ui->middleNameEdit->text().length())
        return 0;
    if(!ui->groupEdit->text().length())
        return 0;
    if(!ui->loginLineEdit->text().length())
        return 0;
    if(!ui->passwordEdit->text().length())
        return 0;
    return 1;
}

void AddUser::callMessageBox(QString str)
{
    QMessageBox box;
    box.setText(str);
    box.setIcon(QMessageBox::Information);
    box.exec();
}

void AddUser::on_cancelButton_clicked()
{
    this->close();
}

void AddUser::on_addButton_clicked()
{
    if(checkInfo()){
        User newUser(ui->nameLineEdit->text(),ui->lastNameEdit->text(),ui->middleNameEdit->text(),ui->groupEdit->text(),ui->loginLineEdit->text(),ui->passwordEdit->text());
        emit add(newUser);

        this->close();
    }
    else{
        this->callMessageBox("Заполните все поля!");
    }
}
