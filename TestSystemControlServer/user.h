#ifndef USER_H
#define USER_H

#include <QString>

class User
{
private:
    QString name,lastName,middleName,nameGroup,login,password;
public:
    User(QString,QString,QString,QString,QString,QString);
    QString getName() const;
    void setName(const QString &value);
    QString getLastName() const;
    void setLastName(const QString &value);
    QString getMiddleName() const;
    void setMiddleName(const QString &value);
    QString getNameGroup() const;
    void setNameGroup(const QString &value);
    QString getLogin() const;
    void setLogin(const QString &value);
    QString getPassword() const;
    void setPassword(const QString &value);
};

#endif // USER_H
