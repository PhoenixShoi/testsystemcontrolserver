#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "user.h"
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QTcpServer>
#include "server.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void add(User);

    void exportFile(QString);

    void deleteUser(QString);

    void on_prevButton_clicked();

    void on_nextButton_clicked();

    void on_offlineTableView_doubleClicked(const QModelIndex &index);

    void on_onlineTableView_doubleClicked(const QModelIndex &index);

    void on_addButton_clicked();

    void loginUser(QString login,QString password);
    void newAnswer(QString login,QString password,QString answer);

    void on_exportButton_clicked();

private:
    Ui::MainWindow *ui;

    void createConnection();
    void insertData(User);
    void updateOffline();
    void updateOnline();
    void fullListUpdate();
    void openUser(QString,QString);
    void userOffline();
    void addAnswerInfo(QString str,int x);

    Server *core;

    QSqlDatabase myDB;
};

#endif // MAINWINDOW_H
