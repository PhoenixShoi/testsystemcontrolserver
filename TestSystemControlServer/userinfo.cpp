#include "userinfo.h"
#include "ui_userinfo.h"

UserInfo::UserInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserInfo)
{
    ui->setupUi(this);

    this->setWindowTitle("Информация о пользователе");
}

void UserInfo::update(User profile)
{
    ui->nameUserLabel->setText(profile.getName());
    ui->lastNameUserLabel->setText(profile.getLastName());
    ui->middleNameUserLabel->setText(profile.getMiddleName());
    ui->groupNameUserLabel->setText(profile.getNameGroup());
    ui->loginUserLabel->setText(profile.getLogin());
    ui->passwordUserLabel->setText(profile.getPassword());
}

UserInfo::~UserInfo()
{
    delete ui;
}

void UserInfo::on_deleteButton_clicked()
{
    emit deleteUser(ui->loginUserLabel->text());
    this->close();
}
