#include "user.h"


QString User::getName() const
{
    return name;
}

void User::setName(const QString &value)
{
    name = value;
}

QString User::getLastName() const
{
    return lastName;
}

void User::setLastName(const QString &value)
{
    lastName = value;
}

QString User::getMiddleName() const
{
    return middleName;
}

void User::setMiddleName(const QString &value)
{
    middleName = value;
}

QString User::getNameGroup() const
{
    return nameGroup;
}

void User::setNameGroup(const QString &value)
{
    nameGroup = value;
}

QString User::getLogin() const
{
    return login;
}

void User::setLogin(const QString &value)
{
    login = value;
}

QString User::getPassword() const
{
    return password;
}

void User::setPassword(const QString &value)
{
    password = value;
}
User::User(QString name,QString lastName,QString middleName,QString nameGroup,QString login,QString password)
{
    this->name=name;
    this->lastName=lastName;
    this->middleName=middleName;
    this->nameGroup=nameGroup;
    this->login=login;
    this->password=password;
}
