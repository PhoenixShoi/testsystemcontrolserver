#include "exportfile.h"
#include "ui_exportfile.h"
#include <QMessageBox>

ExportFile::ExportFile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExportFile)
{
    ui->setupUi(this);

    this->setWindowTitle("Имя файла.");
}

ExportFile::~ExportFile()
{
    delete ui;
}

void ExportFile::on_exportButton_clicked()
{
    if(!ui->lineEdit->text().length()){
        this->callMessageBox("Введите имя файла.");
    }
    else{
        emit exportFile(ui->lineEdit->text());

        this->close();
    }
}

void ExportFile::callMessageBox(QString str)
{
    QMessageBox box;
    box.setText(str);
    box.setIcon(QMessageBox::Information);
    box.exec();
}
