#ifndef ADDUSER_H
#define ADDUSER_H

#include <QDialog>
#include "user.h"

namespace Ui {
class AddUser;
}

class AddUser : public QDialog
{
    Q_OBJECT

public:
    explicit AddUser(QWidget *parent = 0);
    ~AddUser();

private slots:
    void on_loginCheckBox_stateChanged(int arg1);

    void on_passwordCheckBox_stateChanged(int arg1);

    void on_cancelButton_clicked();

    void on_addButton_clicked();

private:
    Ui::AddUser *ui;
    void generateLoginEdit();
    void generatePasswordEdit();
    QString generateString();
    bool checkInfo();
    void callMessageBox(QString str);
signals:
    void add(User);
};

#endif // ADDUSER_H
